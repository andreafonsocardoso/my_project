#include <stdio.h>
#include "dice.h"

int main(){
    int n;
    initializeSeed();                  //Seed
    printf("What's the number of faces of your dice?\n");
    scanf("%d", &n);
    printf("Let's roll the dice: %d\n", rollDice(n));      //imprime o número
    return 0;
}
